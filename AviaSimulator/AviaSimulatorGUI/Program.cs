﻿using System.Windows.Forms;

namespace AviaSimulatorGUI
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.Run(new MainForm());
        }
    }
}
