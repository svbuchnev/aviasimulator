﻿using System;

namespace AviaSimulatorLogic
{
    internal class Airplane
    {
        public readonly string Name;
        public readonly double Speed;
        public readonly double PurchasePrice;
        public readonly double RentalPricePerDay;
        public readonly double RentalPricePerMonth;
        public readonly double LeasingPricePerDay;
        public readonly double LeasingPricePerMonth;
        public readonly double CostPerFlight;
        public readonly double CostPerKm;
        public readonly double Resource;
        public readonly double CarryingCapacity;

        public double RemainingResource { get; private set; }
        public Airport CurrentLocation { get; }
        public double RemainingResourcePercent => Resource / RemainingResource;
        public int PassengerCapacity => (int)Math.Floor(CarryingCapacity * 0.8 / 90);
        public double SellingPrice => PurchasePrice * RemainingResourcePercent;
        
        public bool IsInTheAir => CurrentLocation == null;

        public Airplane(string name, double speed, double purchasePrice, double rentalPricePerDay,
            double rentalPricePerMonth, double leasingPricePerDay, double leasingPricePerMonth, double costPerFlight,
            double costPerKm, double resource, double carryingCapacity)
        {
            Name = name;
            Speed = speed;
            PurchasePrice = purchasePrice;
            RentalPricePerDay = rentalPricePerDay;
            RentalPricePerMonth = rentalPricePerMonth;
            LeasingPricePerDay = leasingPricePerDay;
            LeasingPricePerMonth = leasingPricePerMonth;
            CostPerFlight = costPerFlight;
            CostPerKm = costPerKm;
            Resource = resource;
            CarryingCapacity = carryingCapacity;

            RemainingResource = Resource;
        }



    }
}
