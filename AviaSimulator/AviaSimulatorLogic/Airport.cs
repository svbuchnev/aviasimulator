namespace AviaSimulatorLogic
{
    internal class Airport
    {
        public readonly string Name;
        public readonly double Latitude;
        public readonly double Longitude;

        public Airport(string name, double latitude, double longitude)
        {
            Name = name;
            // TODO: Latitude and longitude constaints
            Latitude = latitude;
            Longitude = longitude;
        }


    }
}
