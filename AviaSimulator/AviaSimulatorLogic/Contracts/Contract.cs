﻿namespace AviaSimulatorLogic.Contracts
{
    internal abstract class Contract
    {
        public readonly Airplane Airplane;
        public readonly PaymentPeriod PaymentPeriod;
        public readonly int NumberOfPeriods;

        public Contract(Airplane airplane, PaymentPeriod paymentPeriod, int numberOfPeriods)
        {
            Airplane = airplane;
            PaymentPeriod = paymentPeriod;
            NumberOfPeriods = numberOfPeriods;
        }


    }
}
