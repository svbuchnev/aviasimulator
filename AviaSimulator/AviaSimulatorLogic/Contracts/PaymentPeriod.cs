﻿namespace AviaSimulatorLogic.Contracts
{
    internal enum PaymentPeriod
    {
        Day,
        Month
    }
}
