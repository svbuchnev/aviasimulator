﻿using System;
using System.Collections.Generic;

namespace AviaSimulatorLogic.Flights
{
    internal class CargoFlight : Flight
    {
        public readonly double RequiredCarryingCapacity;
        
        public CargoFlight(DateTime date, List<DayOfWeek> regularity, bool isRegular, FlightType flightType,
            Airport departurePoint, Airport arrivalPoint, double profit, double penalty,
            double requiredCarryingCapacity) : base(date, regularity, isRegular, flightType, departurePoint,
            arrivalPoint, profit, penalty)
        {
            RequiredCarryingCapacity = requiredCarryingCapacity;
        }


    }
}
