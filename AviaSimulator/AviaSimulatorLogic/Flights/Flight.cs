﻿using System;
using System.Collections.Generic;

namespace AviaSimulatorLogic.Flights
{
    internal abstract class Flight
    {
        public readonly DateTime Date;
        public readonly List<DayOfWeek> Regularity;
        public readonly bool IsRegular;
        public readonly FlightType FlightType;
        public readonly Airport DeparturePoint;
        public readonly Airport ArrivalPoint;
        public readonly double Profit;
        public readonly double Penalty;

        public double Distance { get; }

        public Flight(DateTime date, List<DayOfWeek> regularity, bool isRegular, FlightType flightType,
            Airport departurePoint, Airport arrivalPoint, double profit, double penalty)
        {
            Date = date;
            Regularity = regularity;
            IsRegular = isRegular;
            FlightType = flightType;
            DeparturePoint = departurePoint;
            ArrivalPoint = arrivalPoint;
            Profit = profit;
            Penalty = penalty;
        }

        // If flight is not regular it simply returns value of field "Date"
        // If flight is regular, returns nearest flight date according to regularity
        public DateTime GetNextFlightDate()
        {
            throw new NotImplementedException();
        }


    }
}
