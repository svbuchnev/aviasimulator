﻿namespace AviaSimulatorLogic.Flights
{
    internal enum FlightType
    {
        OneWay,
        RoundTrip
    }
}
