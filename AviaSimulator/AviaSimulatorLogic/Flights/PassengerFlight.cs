﻿using System;
using System.Collections.Generic;

namespace AviaSimulatorLogic.Flights
{
    internal class PassengerFlight : Flight
    {
        public readonly int RequiredCapacity;
        public readonly List<double> ProfitModifier;
        
        public PassengerFlight(DateTime date, List<DayOfWeek> regularity, bool isRegular, FlightType flightType,
            Airport departurePoint, Airport arrivalPoint, double profit, double penalty, int requiredCapacity,
            List<double> profitModifier) : base(date, regularity, isRegular, flightType, departurePoint,
            arrivalPoint, profit, penalty)
        {
            RequiredCapacity = requiredCapacity;
            ProfitModifier = profitModifier;
        }
    }


}
