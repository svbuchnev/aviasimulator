﻿using System;
using System.Collections.Generic;
using System.Timers;
using AviaSimulatorLogic.Contracts;
using AviaSimulatorLogic.Flights;

namespace AviaSimulatorLogic
{
    internal class GameModel
    {
        private const double DeltaMinutes = 1;

        public DateTime CurrentTime { get; }
        public double TimeFlowModifier { get; set; }
        public List<Flight> Flights { get; }
        public List<Airport> Airports { get; }
        public List<Airplane> Airplanes { get; }
        public Dictionary<Flight, Airplane> Schedule { get; set; }
        public double Budget { get; private set; }
        public Airport BaseAirport { get; set; }
        public List<Airplane> OwnedAirplanes { get; }
        public List<Contract> Contracts { get; }
  
        public Timer Timer { get; private set; }

        public GameModel(double budget, Airport baseAirport, List<Airport> airports, List<Airplane> airplanes)
        {
            Budget = budget;
            BaseAirport = baseAirport;
            Airports = airports;
            Airplanes = airplanes;

            Timer = new Timer();
            Timer.Elapsed += TimerOnElapsed;
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            CurrentTime.AddMinutes(DeltaMinutes * TimeFlowModifier);
            throw new NotImplementedException();
        }


    }
}
